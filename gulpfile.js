/***********
 * SETTINGS
 *********/


/** Use Virtual Domain wrapper? **/
var VIRTUAL_DOMAIN = false; // virtual domain or FALSE

/** Paths **/
var ASSETS_PATH = './assets/';

/** Watching Files **/
var PHP_FILES = true;    // Watch PHP Files?
var HTML_FILES = false;  // Watch HTML Files?


/** Frontend Javascript Precompiling - Hard set for CMS **/
var INCLUDE_JQUERY = true; // Whether to include jQuery library
var INCLUDE_JQUERY_UI = false; // Whether to include jQuery UI library

/***********
 * INIT
 *********/
console.time('Loading plugins');
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify-es').default,
    rename = require('gulp-rename'),
    moment = require('moment'),
    notify = require('gulp-notify'),
    scss = require('gulp-sass'),
    serve = require('gulp-serve'),
    cleanCSS = require('gulp-clean-css'),
    stripCssComments = require('gulp-strip-css-comments');

console.timeEnd('Loading plugins');

require('gulp-help')(gulp, {
    description: 'Help listing.'
});

var jquery_path = ASSETS_PATH + 'components/jquery/index.js';
var jquery_ui_path = ASSETS_PATH + 'components/jquery-ui.min/index.js';

gulp.task('uglify-js', 'Concat, Ng-Annotate, Uglify JavaScript into a single app.min.js.', function () {


    // Only compile with jQUery and UI if required
    var jquery_frontend_path = INCLUDE_JQUERY ? jquery_path : '';
    var jquery_frontend_ui_path = INCLUDE_JQUERY_UI ?  jquery_ui_path : '';

    gulp.src([
        jquery_frontend_path, jquery_frontend_ui_path,
        ASSETS_PATH + 'js/compiled/**/*.js', // everything in compiled folder
        ASSETS_PATH + 'js/app.js' // custom app
    ])
        .pipe(concat('app'))
        .pipe(ngAnnotate())
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(uglify())
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(rename({
            extname: ".min.js"
        }))
        .pipe(gulp.dest(ASSETS_PATH + 'js'))
});


gulp.task('scss', 'Compile scss into a single app.css.', function () {
    gulp.src([ASSETS_PATH + 'scss/app.scss'])
        .pipe(concat('app'))
        .pipe(scss())
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(stripCssComments())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(ASSETS_PATH + 'css'))
        .pipe(browserSync.stream());
});


gulp.task('browser-sync', function () {

    if (VIRTUAL_DOMAIN) {
        console.log('Virtual host ' + VIRTUAL_DOMAIN + ' set - Attempting browser sync load');
        browserSync.init({
            proxy: VIRTUAL_DOMAIN
        });
    }
    else  browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    /***** FRONTEND WATCH TASKS *****/
    gulp.watch([ASSETS_PATH + "scss/app.scss", ASSETS_PATH + "scss/components/**/*.scss"], ['scss']);
    gulp.watch([ASSETS_PATH + "js/plugins/*.js", ASSETS_PATH + "js/components/*.js", ASSETS_PATH + "js/app.js"], ['uglify-js']).on('change', browserSync.reload);

    /***** HTML PAGE WATCH TASKS *****/
    if (HTML_FILES)
        gulp.watch(["./**/*.html"]).on('change', browserSync.reload);

    /***** PHP PAGE WATCH TASKS *****/
    if (PHP_FILES)
        gulp.watch(["./**/*.php"]).on('change', browserSync.reload);
});


gulp.task('default', ['uglify-js', 'scss']);
gulp.task('local', ['default', 'browser-sync']);
