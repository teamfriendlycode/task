$(function () {




    // JSON feed URL

    let feedUrl = 'https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?';


    parseFlickr(feedUrl);


});


function parseFlickr(url) {

    $.getJSON(url, {
        tags: "pizza",
        tagmode: "any",
        format: "json"
    })
        .done(function (data) {
            $.each(data.items, function (i, item) {

                // Check what's available for use
                console.log(item);

                $("<div class='image'>")
                    .css('background-image', 'url(' + item.media.m + ')')
                    .appendTo('.photos')
                    .append('<h3 class="title">' + item.title + '</h3>')
                    .wrap("<div class='image-container'>");

                if (i === 17)
                    return false;

            });

        });
}